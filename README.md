# Virtual Wallet
### <Transfer money quick, safe, and free>


## About: 

Web application that enables you to continently manage your budget. 
Every user can send and receive money (user to user) and put money in his Virtual Wallet (bank to app). 
Virtual Wallet has a core set of requirements that are absolute must and a variety of optional features.

## Link to Swagger Documentation: 

http://localhost:8080/swagger-ui/index.html#/

## How to Build and Run the Project:

1. Clone the repository
2. Compile and Run

## How to Create and Fill the Database with Data:

1. Go to database directory
2. Open create.sql file and Copy its content 
3. Paste it in the console and Run it
4. Open insert.sql file and Copy its content
5. Paste it in the console and Run it
6. Enjoy ! :))) 

## Database photo: 

![DatabaseVisualization.png](Images/DatabaseVisualization.png)

## Technologies:

1. Java 11
2. Spring Framework
3. MariaDB
4. Hibernate
5. Mockito
6. Thymeleaf
7. Bootstrap
8. HTML and CSS

