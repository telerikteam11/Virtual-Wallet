INSERT INTO roles (name)
VALUES ('Customer'),
       ('Admin');

INSERT INTO statuses (name)
VALUES ('unblocked'),
       ('blocked');

INSERT INTO users (username, password, email, phone_number, role_id, status_id, photo, enabled,verification_code)
VALUES ('Ivan', 'Pa55@ivan', 'fakeivan@gmail.com', '0881234567', 1, 1, 'profile-0-default.png',1,'6vz46df8h4dzb6ae4214z54'),
       ('Georgi', 'Pa55@georgi', 'fakegeorgi@gmail.com', '0887654321', 1, 1, 'profile-0-default.png',1,'54874835468sef6f4s'),
       ('Peter', 'Pa55@peter', 'fakepeter@gmail.com', '0880123456', 1, 1, 'profile-0-default.png',1,'ae687rg46gb5rtgju87u6sh84t'),
       ('Lyubo', 'Pa55@lyubo', 'lyubomirlb@gmail.com', '0888870018', 2, 1, 'profile-0-default.png',1,'54874835468sef6f4s'),
       ('Svilkata', 'Pa55@svilen', 'svi.stoyanov@gmail.com', '0898913222', 2, 1, 'profile-0-default.png',1,'a4d86gv4b5dfhn46dzs'),
       ('Bianka', 'Pa55@bianka', 'bianka.todorova@gmail.com', '0889934099', 2, 1, 'profile-0-default.png',1,'dfzbz4gb6zdf4b5d4zn64zd');

INSERT INTO virtual_wallets (user_id, balance)
VALUES (1, 1000.01),
       (2, 1000.22),
       (3, 1000.03),
       (4, 1088.88),
       (5, 1055.55),
       (6, 1066.66);

INSERT INTO cards (card_number, expiration_date, holder_name, check_number, user_id)
VALUES (1234567891234567, '2022-01-01', 'Ivan Ivanov', 123, 1),
       (2345678912345678, '2022-02-02', 'Georgi Georgiev', 234, 2),
       (3456789123456789, '2022-03-03', 'Petur Petrov', 345, 3),
       (8888888888888888, '2028-08-08', 'Lyubomir Lyubomirov', 888, 4),
       (6666666666666666, '2026-06-06', 'Svilen Stoyanov', 666, 5),
       (9999999999999999, '2029-03-03', 'Bianka Kostadinova', 999, 6);

INSERT INTO transactions (transaction_date, sender_id, receiver_id, amount)
VALUES ('2021-09-13', 1, 2, 100),
       ('2021-09-14', 2, 3, 200),
       ('2021-09-15', 1, 2, 200),
       ('2021-09-16', 2, 3, 100);

INSERT INTO transfers (transfer_date, card_id, wallet_id, amount)
VALUES ('2021-08-13', 1, 1, 200),
       ('2021-08-14', 2, 2, 300),
       ('2021-08-15', 3, 3, 400),
       ('2021-08-16', 1, 1, 500);

