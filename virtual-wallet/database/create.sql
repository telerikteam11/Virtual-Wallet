create schema virtual_wallet;

use virtual_wallet;
create or replace table roles
(
    role_id int auto_increment
        primary key,
    name varchar(20) not null,
    constraint roles_name_uindex
        unique (name)
);

create or replace table statuses
(
    status_id int auto_increment
        primary key,
    name varchar(30) not null
);

create or replace table users
(
    status_id int null,
    photo varchar(300) null,
    user_id int auto_increment
        primary key,
    username varchar(20) not null,
    password varchar(100) not null,
    email varchar(30) not null,
    phone_number varchar(10) not null,
    role_id int null,
    verification_code varchar(64) not null,
    enabled tinyint(1) default 1 null,
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_username_uindex
        unique (username),
    constraint users_roles_fk
        foreign key (role_id) references roles (role_id),
    constraint users_statuses_fk
        foreign key (status_id) references statuses (status_id)
);

create or replace table cards
(
    card_id int auto_increment
        primary key,
    card_number varchar(16) not null,
    expiration_date date not null,
    holder_name varchar(30) not null,
    check_number int not null,
    user_id int null,
    constraint cards_card_number_uindex
        unique (card_number),
    constraint cards_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table virtual_wallets
(
    wallet_id int auto_increment
        primary key,
    user_id int null,
    balance double default 0 null,
    constraint virtual_wallets_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table transactions
(
    transaction_id int auto_increment
        primary key,
    transaction_date date null,
    sender_id int null,
    receiver_id int null,
    amount double not null,
    constraint transactions_wallet_receiver_fk
        foreign key (receiver_id) references virtual_wallets (wallet_id),
    constraint transactions_wallet_sender_fk
        foreign key (sender_id) references virtual_wallets (wallet_id)
);

create or replace table transfers
(
    transfer_id int auto_increment
        primary key,
    transfer_date date null,
    card_id int null,
    wallet_id int null,
    amount double not null,
    constraint transfers_cards_fk
        foreign key (card_id) references cards (card_id),
    constraint transfers_wallets_fk
        foreign key (wallet_id) references virtual_wallets (wallet_id)
);

