package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.models.dtos.BankDummyDto;
import com.telerikacademy.virtualwallet.services.contracts.BankDummyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/api/bankdummy")
public class BankDummyController {

    private final BankDummyService bankDummyService;

    @Autowired
    public BankDummyController(BankDummyService bankDummyService) {
        this.bankDummyService = bankDummyService;
    }

    @PostMapping
    public void depositMoney(@RequestBody BankDummyDto dto){
        if(bankDummyService.depositMoney(stringToLocalDate(dto.getExpirationDate()), dto.getAmount())){
            throw new ResponseStatusException(HttpStatus.OK);
        }else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    private LocalDate stringToLocalDate(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        YearMonth localDate = YearMonth.parse(date,formatter);
        return localDate.atEndOfMonth();
    }

}

