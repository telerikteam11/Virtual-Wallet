package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.TransactionSearchParams;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.mappers.TransactionMapper;
import com.telerikacademy.virtualwallet.services.contracts.TransactionService;
import com.telerikacademy.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

    private final TransactionService transactionService;
    private final WalletService walletService;
    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TransactionController(TransactionService transactionService, WalletService walletService,
                                 TransactionMapper transactionMapper, AuthenticationHelper authenticationHelper) {
        this.transactionService = transactionService;
        this.walletService = walletService;
        this.transactionMapper = transactionMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{id}")
    public Transaction getById(@PathVariable int id) {
        try {
            return transactionService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/my")
    public List<Transaction> getAllUserTransactions(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            String username = headers.getFirst("Authorization");
            return transactionService.getAllUserTransactions(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping
    public List<Transaction> getAll() {
        return transactionService.getAll();
    }



    @GetMapping("/filter")
    public List<Transaction> filter(@RequestHeader HttpHeaders headers,
                                    @RequestParam(required = false) String fromDate,
                                    @RequestParam(required = false) String toDate,
                                    @RequestParam(required = false) String sender,
                                    @RequestParam(required = false) String receiver,
                                    @RequestParam(required = false) String order) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);

            LocalDate dateFrom = LocalDate.EPOCH;
            if (fromDate != null)
                dateFrom = LocalDate.parse(fromDate);

            LocalDate dateTo = LocalDate.now();
            if (toDate != null)
                dateTo = LocalDate.parse(toDate);

            if (dateTo.isBefore(dateFrom))
                throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "toDate is before fromDate!");

            return transactionService.filter(new TransactionSearchParams(dateFrom, dateTo, sender, receiver, order), userAuthenticator);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Transaction create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TransactionDto transactionDto) {
        try {
            User initiator = authenticationHelper.tryGetUser(headers);
            Transaction transactionToCreate = transactionMapper.fromDto(transactionDto);
            transactionService.create(transactionToCreate, initiator);
            return transactionToCreate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
