package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.dtos.MultipartFileDto;
import com.telerikacademy.virtualwallet.models.dtos.UserUpdateDto;
import com.telerikacademy.virtualwallet.models.entity.Role;
import com.telerikacademy.virtualwallet.models.entity.Status;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.mappers.UserMapper;
import com.telerikacademy.virtualwallet.models.mappers.UserRegisterMapper;
import com.telerikacademy.virtualwallet.models.mappers.UserUpdateMvcMapper;
import com.telerikacademy.virtualwallet.utils.FileUploader;
import com.telerikacademy.virtualwallet.services.contracts.RoleService;
import com.telerikacademy.virtualwallet.services.contracts.StatusService;
import com.telerikacademy.virtualwallet.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final StatusService statusService;
    private final FileUploader fileUploader;
    private final UserRegisterMapper registerMapper;
    private final UserUpdateMvcMapper userUpdateMapper;
    private final RoleService roleService;

    @Autowired
    public UserMvcController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper, StatusService statusService, FileUploader fileUploader, UserRegisterMapper registerMapper, UserUpdateMvcMapper userUpdateMapper, RoleService roleService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.statusService = statusService;
        this.fileUploader = fileUploader;
        this.registerMapper = registerMapper;
        this.userUpdateMapper = userUpdateMapper;
        this.roleService = roleService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("roles")
    public List<Role> populateRoles() {
        return roleService.getAll();
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusService.getAll();
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("users", userService.getAll(user));
            model.addAttribute("currentUser",user);
            return "users";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            User userToGet = userService.getById(id, user);
            model.addAttribute("user", userToGet);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id, initiator);
            UserUpdateDto userDto = userUpdateMapper.toMvcDto(user);
            model.addAttribute("userId", id);
            model.addAttribute("currentUser", user);
            model.addAttribute("userToUpdate", userDto);
            model.addAttribute("file", new MultipartFileDto());
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id, @Valid UserUpdateDto userDto,
                                 BindingResult errors, Model model, HttpSession session) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "user-update";
        }

        try {
            User user = userUpdateMapper.fromDto(userDto, id);
            userService.update(user, initiator);
//            return "redirect:/users/" + id;
            return "redirect:/users/"+id+"/update";
//            return  "redirect:/";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("username", "duplicate_users", e.getMessage());
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.delete(id, initiator);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/search")
    public String search(Model model, HttpSession session, @RequestParam(required = false) Optional<String> searchTerm) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            List<User> users = userService.search(searchTerm);
            model.addAttribute("users", users);
            return "users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/update-status/{id}")
    public String updateCustomerStatus(@PathVariable int id, Model model, HttpSession session) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User customer = userService.getById(id, initiator);
            userService.updateStatus(customer, initiator);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/update-photo/{id}")
    public String updatePhoto(@PathVariable int id,
                                MultipartFileDto multipartFileDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {

        if (errors.hasErrors()) {
            return "user-update";
        }

        try {
            User initiator = authenticationHelper.tryGetUser(session);
            fileUploader.uploadFile(multipartFileDto.getFile());
            User user = userService.getById(id, initiator);
            user.setPhoto(multipartFileDto.getFile().getOriginalFilename());
            initiator.setPhoto(multipartFileDto.getFile().getOriginalFilename());
            userService.update(user, initiator);
            return "redirect:/users/" + id + "/update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (IOException e) {
            return "not-found";
        }
    }

}
