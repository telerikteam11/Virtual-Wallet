package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.dtos.CardDto;
import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.mappers.CardMapper;
import com.telerikacademy.virtualwallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/cards")
public class CardMvcController {

    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardMvcController(CardService cardService, CardMapper cardMapper, AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/my")
    public String showUserCards(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            String username = user.getUsername();
            model.addAttribute("cards", cardService.getAllUserCards(username));
            model.addAttribute("currentUser", user);
            return "cards-my";
        } catch (AuthenticationFailureException e) {
            return "access-denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleCard(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Card card = cardService.getById(id, user);
            model.addAttribute("card", card);
            model.addAttribute("currentUser", user);
            return "card";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String ShowNewCardPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", user);
            model.addAttribute("card", new CardDto());
            return "card-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/new")
    public String createCard(@Valid @ModelAttribute("card") CardDto cardDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "card-new";
        }

        try {
            Card card = cardMapper.fromDto(cardDto);
            cardService.create(card, user);
            return "redirect:/cards/my";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("card", "duplicate_card", e.getMessage());
            return "card-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditCardPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Card card = cardService.getById(id, user);
            CardDto cardDto = cardMapper.toDto(card);
            model.addAttribute("cardId", id);
            model.addAttribute("card", cardDto);
            model.addAttribute("currentUser", user);
            return "card-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{id}/update")
    public String updateCard(@PathVariable int id,
                             @Valid @ModelAttribute("card") CardDto cardDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "card-update";
        }

        try {
            Card card = cardMapper.fromDto(cardDto, id);
            cardService.update(card, user);
            return "redirect:/cards/my";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("card", "duplicate_card", e.getMessage());
            return "card-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCard(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            cardService.delete(id, user);
            return "redirect:/cards/my";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

}

