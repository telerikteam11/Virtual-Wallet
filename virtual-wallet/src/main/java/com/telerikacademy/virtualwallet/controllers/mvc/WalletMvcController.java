package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.dtos.WalletDto;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.entity.Wallet;
import com.telerikacademy.virtualwallet.models.mappers.WalletMapper;
import com.telerikacademy.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/wallets")
public class WalletMvcController {

    private final WalletService walletService;
    private final WalletMapper walletMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WalletMvcController(WalletService walletService,
                               WalletMapper walletMapper,
                               AuthenticationHelper authenticationHelper) {
        this.walletService = walletService;
        this.walletMapper = walletMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/my")
    public String showUserWallets(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            String username = user.getUsername();
            model.addAttribute("wallets", walletService.getAllUserWallets(username));
            model.addAttribute("currentUser", user);
            return "wallets-my";
        } catch (AuthenticationFailureException e) {
            return "access-denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleWallet(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Wallet wallet = walletService.getById(id, user);
            model.addAttribute("wallet", wallet);
            return "wallet";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String ShowNewWalletPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", user);
            model.addAttribute("wallet", new WalletDto());
            return "wallet-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/new")
    public String createWallet(@Valid @ModelAttribute("wallet") WalletDto walletDto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "wallet-new";
        }

        try {
            Wallet wallet = walletMapper.fromDto(walletDto);
            walletService.create(wallet, user);
            return "redirect:/wallets/my";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("wallet", "duplicate_wallet", e.getMessage());
            return "wallet-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditWalletPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Wallet wallet = walletService.getById(id, user);
            WalletDto walletDto = walletMapper.toDto(wallet);
            model.addAttribute("walletId", id);
            model.addAttribute("wallet", walletDto);
            return "wallet-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{id}/update")
    public String updateWallet(@PathVariable int id,
                               @Valid @ModelAttribute("wallet") WalletDto walletDto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "wallet-update";
        }

        try {
            Wallet wallet = walletMapper.fromDto(walletDto, id);
            walletService.update(wallet, user);
            return "redirect:/wallets/my";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("wallet", "duplicate_wallet", e.getMessage());
            return "wallets-my";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteWallet(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            walletService.delete(id, user);
            return "redirect:/wallets/my";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

}

