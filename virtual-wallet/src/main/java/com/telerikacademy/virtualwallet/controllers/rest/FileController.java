package com.telerikacademy.virtualwallet.controllers.rest;

import com.telerikacademy.virtualwallet.utils.FileUploader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/files")
public class FileController {
    private final FileUploader uploader;

    @Autowired
    public FileController(FileUploader uploader) {
        this.uploader = uploader;
    }

    @PostMapping
    public void uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        uploader.uploadFile(file);
    }
}
