package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.entity.Wallet;
import com.telerikacademy.virtualwallet.models.mappers.TransactionMapper;
import com.telerikacademy.virtualwallet.services.contracts.TransactionService;
import com.telerikacademy.virtualwallet.services.contracts.UserService;
import com.telerikacademy.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/transactions")
public class TransactionMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final WalletService walletService;
    private final UserService userService;

    @Autowired
    public TransactionMvcController(AuthenticationHelper authenticationHelper, TransactionService transactionService,
                                    TransactionMapper transactionMapper, WalletService walletService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.walletService = walletService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("myWallets")
    public List<Wallet> populateMyWallets(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return walletService.getAllUserWallets(user.getUsername());
    }

    @ModelAttribute("wallets")
    public List<Wallet> populateWallets() {
        return walletService.getAll();
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.populate();
    }

    @GetMapping("/{id}")
    public String showSingleTransaction(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Transaction transaction = transactionService.getById(id);
            model.addAttribute("transaction", transaction);
            return "transaction";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping
    public String showAllTransactions(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("transactions", transactionService.getAll());
            model.addAttribute("currentUser", user);
            return "transactions";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/my")
    public String showUserTransactions(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            String username = user.getUsername();
            model.addAttribute("transactions", transactionService.getAllUserTransactions(username));
            model.addAttribute("currentUser", user);
            return "transactions-my";
        } catch (AuthenticationFailureException e) {
            return "access-denied";
        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
    }

    @GetMapping("/new")
    public String showNewTransactionPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("currentUser", user);
        model.addAttribute("transaction", new TransactionDto());
        return "transaction-new";
    }

    @PostMapping("/new")
    public String createTransaction(@Valid @ModelAttribute("transaction") TransactionDto transactionDto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {

        if (errors.hasErrors()) {
            return "transaction-new";
        }

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Transaction transaction = transactionMapper.fromDto(transactionDto);
            transactionService.create(transaction, user);
            return "redirect:/transactions/my";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("id", "duplicate_transaction", e.getMessage());
            return "transaction-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        }
    }

}
