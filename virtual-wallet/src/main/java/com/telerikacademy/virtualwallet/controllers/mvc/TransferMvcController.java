package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.*;
import com.telerikacademy.virtualwallet.models.dtos.TransferDto;
import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.Transfer;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.entity.Wallet;
import com.telerikacademy.virtualwallet.models.mappers.TransferMapper;
import com.telerikacademy.virtualwallet.services.contracts.CardService;
import com.telerikacademy.virtualwallet.services.contracts.TransferService;
import com.telerikacademy.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/transfers")
public class TransferMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final TransferService transferService;
    private final TransferMapper transferMapper;
    private final WalletService walletService;
    private final CardService cardService;

    @Autowired
    public TransferMvcController(AuthenticationHelper authenticationHelper,
                                 TransferService transferService,
                                 TransferMapper transferMapper,
                                 WalletService walletService,
                                 CardService cardService) {
        this.authenticationHelper = authenticationHelper;
        this.transferService = transferService;
        this.transferMapper = transferMapper;
        this.walletService = walletService;
        this.cardService = cardService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("myWallets")
    public List<Wallet> populateMyWallets(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return walletService.getAllUserWallets(user.getUsername());
    }

    @ModelAttribute("myCards")
    public List<Card> populateMyCards(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        return cardService.getAllUserCards(user.getUsername());
    }

    @GetMapping("/{id}")
    public String showSingleTransfer(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Transfer transfer = transferService.getById(id); //add user
            model.addAttribute("transfer", transfer);
            return "transfer";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping
    public String showAllTransfers(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("transfers", transferService.getAll()); //add user
            model.addAttribute("currentUser", user);
            return "transfers";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/my")
    public String showUserTransfers(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            String username = user.getUsername();
            model.addAttribute("transfers", transferService.getAllUserTransfers(username));
            model.addAttribute("currentUser", user);
            return "transfers-my";
        } catch (AuthenticationFailureException e) {
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String showNewTransferPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("transfer", new TransferDto());
        model.addAttribute("currentUser", user);
        return "transfer-new";
    }

    @PostMapping("/new")
    public String createTransfer(@Valid @ModelAttribute("transfer") TransferDto transferDto,
                                    BindingResult errors,
                                    Model model,
                                    HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "transfer-new";
        }

        try {
            Transfer transfer = transferMapper.fromDto(transferDto);
            try {
                transferService.create(transfer, user);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "redirect:/transfers/my";

        } catch (DuplicateEntityException e) {
            errors.rejectValue("transfer", "duplicate_transfer", e.getMessage());
            return "transfer-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (TransferFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "unavailable";
        }
    }

}
