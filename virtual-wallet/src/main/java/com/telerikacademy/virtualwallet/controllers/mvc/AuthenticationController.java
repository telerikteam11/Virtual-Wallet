package com.telerikacademy.virtualwallet.controllers.mvc;

import com.telerikacademy.virtualwallet.controllers.AuthenticationHelper;
import com.telerikacademy.virtualwallet.exceptions.AuthenticationFailureException;
import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.models.dtos.LoginDto;
import com.telerikacademy.virtualwallet.models.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.mappers.UserMapper;
import com.telerikacademy.virtualwallet.models.mappers.UserRegisterMapper;
import com.telerikacademy.virtualwallet.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserRegisterMapper userRegisterMapper;


    @Autowired
    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserRegisterMapper userRegisterMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userRegisterMapper = userRegisterMapper;

    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {

        model.addAttribute("login", new LoginDto());
        return "auth-login-basic";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "auth-login-basic";
        }

        try {
            User currentUser = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            session.setAttribute("currentUserId",currentUser.getId());

            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "auth-login-basic";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        session.removeAttribute("currentUserId");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "auth-register-basic";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult,
                                 HttpServletRequest request) throws MessagingException, UnsupportedEncodingException {
        if (bindingResult.hasErrors()) {
            return "auth-register-basic";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "auth-register-basic";
        }

        try {
            User user = userRegisterMapper.fromDto(register);
            userService.create(user);
            String siteURL = request.getRequestURL().toString().replace(request.getServletPath(), "");
            userService.sendVerificationEmail(user, siteURL);

            return "auth-register-success";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "auth-register-basic";
        }
    }

    @GetMapping("/verify")
    public String verifyAccount(@Param("code") String code) {
        if (userService.verify(code)) {
            return "auth-verify-success";
        } else {
            return "auth-verify-fail";
        }
    }

}
