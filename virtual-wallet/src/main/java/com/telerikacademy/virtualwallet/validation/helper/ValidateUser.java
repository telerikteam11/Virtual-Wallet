package com.telerikacademy.virtualwallet.validation.helper;

import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.entity.User;

public class ValidateUser {

    private static final String USER_BLOCKED_MESSAGE = "The user is currently blocked and unable to make transactions.";
    private static final String USER_NOT_CUSTOMER_MESSAGE = "Only registered users are authorized.";
    private static final String USER_NOT_ADMIN_MESSAGE = "Only administrators are authorized.";
    private static final String USER_NOT_CUSTOMER_AND_ADMIN_MESSAGE = "Only registered users and administrators are authorized.";
    private static final String USER_NOT_OWNER_ERROR_MESSAGE = "Cannot modify other users";

    public static void validateIsAdminOrCustomer(User user) {
        if (!user.isAdmin() && !user.isCustomer()) {
            throw new UnauthorizedOperationException(USER_NOT_CUSTOMER_AND_ADMIN_MESSAGE);
        }
    }

    public static void validateIsAdmin(User user) {
        if (!user.isAdmin()) {
            throw new UnauthorizedOperationException(USER_NOT_ADMIN_MESSAGE);
        }
    }

    public static void validateIsCustomer(User user) {
        if (!user.isCustomer()) {
            throw new UnauthorizedOperationException(USER_NOT_CUSTOMER_MESSAGE);
        }
    }

    public static void validateIsUnblocked(User user) {
        if (user.isBlocked()) {
            throw new UnauthorizedOperationException(USER_BLOCKED_MESSAGE);
        }
    }

    public static void validateIsSameUser(User userToUpdate, User user) {
        if (!userToUpdate.equals(user)) {
            throw new UnauthorizedOperationException(USER_NOT_OWNER_ERROR_MESSAGE);
        }
    }

}
