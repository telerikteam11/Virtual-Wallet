package com.telerikacademy.virtualwallet.validation.helper;

import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.models.entity.Transaction;

public class ValidateTransaction {

    private static final String INSUFFICIENT_FUNDS_MESSAGE = "Insufficient funds in wallet.";

    public static void validateWalletBalance(Transaction transaction) {
        if (transaction.getSender().getBalance() < transaction.getAmount()) {
            throw new InsufficientFundsException(INSUFFICIENT_FUNDS_MESSAGE);
        }
    }

}
