package com.telerikacademy.virtualwallet.validation.helper;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.entity.Wallet;

public class ValidationHelper {

    public static <T> void validateEntityExists(boolean entityExists, T entity, String errorMessage) {
        if (entityExists) {
            throw new DuplicateEntityException("%s, %s", entity.getClass().getSimpleName(), errorMessage);
        }
    }

    public static <T> void validateEntityDoesNotExists(boolean entityExists, T entity, String errorMessage) {
        if (!entityExists) {
            throw new EntityNotFoundException("%s, %s", entity.getClass().getSimpleName(), errorMessage);
        }
    }

    public static void validateUserExists(boolean userExists, User user) {
        String errorMessage = user.getEmail();
        validateEntityExists(userExists, user, errorMessage);
    }

    public static void validateUserDoesNotExists(boolean userExists, User user) {
        String errorMessage = user.getEmail();
        validateEntityDoesNotExists(userExists, user, errorMessage);
    }

    public static void validateWalletExists(boolean walletExists, Wallet wallet) {
        int walletId = wallet.getId();
        validateEntityExists(walletExists, wallet, String.valueOf(walletId));
    }

    public static void validateWalletDoesNotExists(boolean walletExists, Wallet wallet) {
        int walletId = wallet.getId();
        validateEntityDoesNotExists(walletExists, wallet, String.valueOf(walletId));
    }

}
