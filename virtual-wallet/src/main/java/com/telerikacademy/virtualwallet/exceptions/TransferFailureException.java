package com.telerikacademy.virtualwallet.exceptions;

public class TransferFailureException extends RuntimeException {

    public TransferFailureException(String message) {
        super(message);
    }

}
