package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.models.entity.Role;
import com.telerikacademy.virtualwallet.repositories.contracts.RoleRepository;
import com.telerikacademy.virtualwallet.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository repository;

    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Role> getAll() {
        return repository.getAll();
    }

    @Override
    public Role getById(int id) {
        return repository.getById(id);
    }

}
