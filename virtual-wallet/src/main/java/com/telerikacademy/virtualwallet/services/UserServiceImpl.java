package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import com.telerikacademy.virtualwallet.services.contracts.UserService;
import com.telerikacademy.virtualwallet.validation.helper.ValidateUser;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.virtualwallet.validation.helper.ValidationHelper.validateUserExists;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final JavaMailSender mailSender;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           JavaMailSender mailSender) {
        this.userRepository = userRepository;
        this.mailSender = mailSender;

    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByField("username", username);
    }

    @Override
    public List<User> getAll(User user) {
        ValidateUser.validateIsAdmin(user);
        return userRepository.getAll();
    }

    @Override
    public List<User> populate() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id, User user) {
        ValidateUser.validateIsAdminOrCustomer(user);
        return userRepository.getById(id);
    }

    @Override
    public List<User> search(Optional<String> keyword) {
        return userRepository.search(keyword);
    }

    @Override
    public void create(User user) {
        boolean userExists = true;

        try {
            userRepository.getByField("username", user.getUsername());
        } catch (EntityNotFoundException e) {
            userExists = false;
        }
        validateUserExists(userExists, user);
        user.setEnabled(false);
        String randomCode = RandomString.make(64);
        user.setVerificationCode(randomCode);

        userRepository.create(user);
    }

    @Override
    public void sendVerificationEmail(User user, String siteURL) throws MessagingException, UnsupportedEncodingException {
        String subject = "Please verify your registration";
        String senderName = "Virtual Wallet";
        String verifyURL = siteURL + "/auth/verify?code=" + user.getVerificationCode();
        String mailContent = "Dear " + user.getUsername() + ", \n" +
                "Please click the link below to verify your registration! \n" +
                "<a href=\"" + verifyURL + "\">VERIFY</a> \n" +
                "Thank you. \n" +
                "The Virtual Wallet Team";
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("vwallet11@gmail.com", senderName);
        helper.setTo(user.getEmail());
        helper.setSubject(subject);

        helper.setText(mailContent, true);

        mailSender.send(message);
    }

    @Override
    public void update(User userToUpdate, User user) {
        ValidateUser.validateIsSameUser(userToUpdate, user);

        userRepository.update(userToUpdate);
    }

    @Override
    public void delete(int id, User user) {
        ValidateUser.validateIsAdmin(user);
        userRepository.delete(id);
    }

    @Override
    public void updateStatus(User userToUpdate, User user) {
        ValidateUser.validateIsAdmin(user);

        userToUpdate.changeStatus();
        userRepository.update(userToUpdate);
    }

    @Override
    public boolean verify(String verificationCode) {
        User user = userRepository.findByVerificationCode(verificationCode);
        if (user == null || user.isEnabled()) {
            return false;
        } else {
            user.setEnabled(true);
            userRepository.update(user);
            return true;
        }
    }

}
