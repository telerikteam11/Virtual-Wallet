package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.entity.Wallet;
import com.telerikacademy.virtualwallet.repositories.contracts.WalletRepository;
import com.telerikacademy.virtualwallet.services.contracts.WalletService;
import com.telerikacademy.virtualwallet.validation.helper.ValidateUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.virtualwallet.validation.helper.ValidationHelper.validateWalletDoesNotExists;
import static com.telerikacademy.virtualwallet.validation.helper.ValidationHelper.validateWalletExists;

@Service
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }


    @Override
    public Wallet getById(int id, User user) {
        return walletRepository.getById(id);
    }

    @Override
    public Wallet getByUserId(int userId) {
        return walletRepository.getByField("User Id", userId);
    }

    @Override
    public List<Wallet> getAll() {
        return walletRepository.getAll();
    }

    @Override
    public List<Wallet> getAllUserWallets(String username) {
        return walletRepository.getAllUserWallets(username);
    }

    @Override
    public void create(Wallet wallet,User initiator) {
        ValidateUser.validateIsAdminOrCustomer(initiator);
        boolean walletExists = true;

        try {
            walletRepository.getById(wallet.getId());
        } catch (EntityNotFoundException e) {
            walletExists = false;
        }
        validateWalletExists(walletExists, wallet);

        walletRepository.create(wallet);
    }

    @Override
    public void withdraw(Wallet wallet, double amount) {
        wallet.setBalance(wallet.getBalance() - amount);
        walletRepository.update(wallet);
    }

    @Override
    public void deposit(Wallet wallet, double amount) {
        wallet.setBalance(wallet.getBalance() + amount);
        walletRepository.update(wallet);
    }

    @Override
    public void update(Wallet wallet,User initiator) {
        ValidateUser.validateIsCustomer(initiator);
        boolean walletExists = true;

        try {
            walletRepository.getById(wallet.getId());
        } catch (EntityNotFoundException e) {
            walletExists = false;
        }
        validateWalletDoesNotExists(walletExists, wallet);

        walletRepository.update(wallet);
    }

    @Override
    public void delete(int id,User initiator) {
        ValidateUser.validateIsAdminOrCustomer(initiator);
        Wallet wallet = walletRepository.getById(id);
        if (wallet.getBalance() != 0) {
            throw new UnauthorizedOperationException("Please transfer all funds to another wallet before deletion!");
        }
        walletRepository.delete(id);
    }

}
