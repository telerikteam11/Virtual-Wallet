package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.CardRepository;
import com.telerikacademy.virtualwallet.services.contracts.CardService;
import com.telerikacademy.virtualwallet.validation.helper.ValidateUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public List<Card> getAll() {
        return cardRepository.getAll();
    }

    @Override
    public Card getById(int id, User user) {
        return cardRepository.getById(id);
    }

    @Override
    public List<Card> getAllUserCards(String username) {
        return cardRepository.getAllUserCards(username);
    }

    @Override
    public void create(Card card, User initiator) {
        ValidateUser.validateIsAdminOrCustomer(initiator);

        boolean duplicateExists = true;

        try {
            cardRepository.getById(card.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Card", "number", card.getCardNumber());
        }
        cardRepository.create(card);
    }

    @Override
    public void update(Card card,User initiator) {
        ValidateUser.validateIsCustomer(initiator);
        boolean cardExists = true;

        try {
            cardRepository.getByField(card.getCardNumber(), card.getCardNumber());
        } catch (EntityNotFoundException e) {
            cardExists = false;
        }
        if (!cardExists) {
            throw new EntityNotFoundException("Card", "number", card.getCardNumber());
        }

        cardRepository.update(card);
    }

    @Override
    public void delete(int id,User initiator) {
        ValidateUser.validateIsAdminOrCustomer(initiator);
        cardRepository.delete(id);
    }

}
