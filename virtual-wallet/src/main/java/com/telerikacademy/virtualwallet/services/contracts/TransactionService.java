package com.telerikacademy.virtualwallet.services.contracts;

import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.TransactionSearchParams;
import com.telerikacademy.virtualwallet.models.entity.User;

import java.util.List;

public interface TransactionService {

    void create(Transaction transaction, User user);

    List<Transaction> filter(TransactionSearchParams searchParameters, User initiator);

    Transaction getById(int id);

    List<Transaction> getAll();

    List<Transaction> getAllUserTransactions(String username);

}
