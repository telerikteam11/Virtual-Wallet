package com.telerikacademy.virtualwallet.services.contracts;

import java.time.LocalDate;

public interface BankDummyService {

    boolean depositMoney(LocalDate expDate, double amount);
}
