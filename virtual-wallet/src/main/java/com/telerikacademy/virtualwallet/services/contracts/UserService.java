package com.telerikacademy.virtualwallet.services.contracts;

import com.telerikacademy.virtualwallet.models.entity.User;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    void create(User user);

    void update(User userToUpdate, User user);

    void delete(int id, User user);

    void updateStatus(User initiator, User customer);

    void sendVerificationEmail(User user,String siteURL) throws MessagingException, UnsupportedEncodingException;

    User getByUsername(String username);

    List<User> getAll(User user);

    User getById(int id, User user);

    List<User> search(Optional<String> keyword);

    List<User> populate();

    boolean verify(String code);
}
