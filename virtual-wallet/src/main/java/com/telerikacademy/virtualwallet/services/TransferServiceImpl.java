package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.TransferFailureException;
import com.telerikacademy.virtualwallet.models.entity.Transfer;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.TransferRepository;
import com.telerikacademy.virtualwallet.services.contracts.TransferService;
import com.telerikacademy.virtualwallet.services.contracts.WalletService;
import com.telerikacademy.virtualwallet.utils.DummyBank;
import com.telerikacademy.virtualwallet.validation.helper.ValidateUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class TransferServiceImpl implements TransferService {

    private final TransferRepository transferRepository;
    private final WalletService walletService;

    @Autowired
    public TransferServiceImpl(TransferRepository transferRepository, WalletService walletService) {
        this.transferRepository = transferRepository;
        this.walletService = walletService;
    }

    @Override
    public List<Transfer> getAll() {
        return transferRepository.getAll();
    }

    public List<Transfer> getAllUserTransfers(String username) {
        return transferRepository.getAllUserTransfers(username);
    }

    @Override
    public Transfer getById(int id) {
        return transferRepository.getById(id);
    }

    @Override
    public void create(Transfer transfer, User user) throws IOException {
        ValidateUser.validateIsAdminOrCustomer(user);
        String cardNumber = transfer.getCard().getCardNumber();
        String cardCheck = transfer.getCard().getCheckNumber();
        String cardExpDate = transfer.getCard().getExpirationDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String amount = String.valueOf(transfer.getAmount());

//        URL url = new URL("http://localhost:8080/api/bankdummy");
//        HttpURLConnection con = (HttpURLConnection) url.openConnection();
//        con.setRequestMethod("POST");
//
//        con.setRequestProperty("Content-Type", "application/json; utf-8");
//        con.setRequestProperty("Accept", "application/json");
//        con.setRequestProperty("Authorization", user.getUsername());
//        con.setDoOutput(true);
//        String jsonInputString = String.format
//                ("{\"cardNumber\": \"%s\"," +
//                        " \"cardCheck\": \"%s\"," +
//                        " \"expirationDate\": \"%s\"," +
//                        " \"amount\": %s}",cardNumber,cardCheck,cardExpDate,amount);
//
//        try(OutputStream os = con.getOutputStream()) {
//            byte[] input = jsonInputString.getBytes("utf-8");
//            os.write(input, 0, input.length);
//        }
//
//        int statusCode = con.getResponseCode();

        int statusCode = DummyBank.dummyTransfer(user.getUsername(), cardNumber, cardCheck, cardExpDate, amount);
        if(statusCode == 200){
            transferRepository.create(transfer);
            walletService.deposit(transfer.getWallet(),transfer.getAmount());
        } else {
            throw new TransferFailureException("Transfer rejected!");
        }
    }

}

