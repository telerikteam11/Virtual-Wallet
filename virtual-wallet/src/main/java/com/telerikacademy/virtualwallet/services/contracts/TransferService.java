package com.telerikacademy.virtualwallet.services.contracts;


import com.telerikacademy.virtualwallet.models.entity.Transfer;
import com.telerikacademy.virtualwallet.models.entity.User;

import java.io.IOException;
import java.util.List;

public interface TransferService {

    void create(Transfer transfer, User user) throws IOException;

    List<Transfer> getAll();

    Transfer getById(int id);

    List<Transfer> getAllUserTransfers(String username);
}