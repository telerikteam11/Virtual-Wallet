package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.services.contracts.BankDummyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class BankDummyServiceImpl implements BankDummyService {

    @Autowired
    public BankDummyServiceImpl() {
    }

    @Override
    public boolean depositMoney(LocalDate expDate, double amount) {
        if (expDate.isBefore(LocalDate.now())){
            return false;
        }
        double random = Math.random()*100;
        return !(random > amount);
    }
}

