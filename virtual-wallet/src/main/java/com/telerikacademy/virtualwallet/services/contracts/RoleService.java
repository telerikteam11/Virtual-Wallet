package com.telerikacademy.virtualwallet.services.contracts;

import com.telerikacademy.virtualwallet.models.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role getById(int id);

}
