package com.telerikacademy.virtualwallet.services.contracts;

import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.Transfer;
import com.telerikacademy.virtualwallet.models.entity.User;

import java.io.IOException;
import java.util.List;

public interface CardService {

    void create(Card card, User user);

    void update(Card card, User user);

    void delete(int id, User user);

    List<Card> getAll();

    Card getById(int id, User user);

    List<Card> getAllUserCards(String username);

}
