package com.telerikacademy.virtualwallet.services.contracts;

import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.entity.Wallet;

import java.util.List;

public interface WalletService {

    void create(Wallet wallet, User user);

    void update(Wallet wallet,User user);

    void delete(int id,User user);

    void withdraw(Wallet wallet, double amount);

    void deposit(Wallet wallet, double amount);

    List<Wallet> getAll();

    Wallet getById(int id, User user);

    Wallet getByUserId(int userId);

    List<Wallet> getAllUserWallets(String username);

}
