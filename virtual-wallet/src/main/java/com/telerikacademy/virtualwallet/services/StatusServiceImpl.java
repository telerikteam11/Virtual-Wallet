package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.models.entity.Status;
import com.telerikacademy.virtualwallet.repositories.contracts.StatusRepository;
import com.telerikacademy.virtualwallet.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getAll() {
        return statusRepository.getAll();
    }

    @Override
    public Status getById(int id) {
        return statusRepository.getById(id);
    }

}
