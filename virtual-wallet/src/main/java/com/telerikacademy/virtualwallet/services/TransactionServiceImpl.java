package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.TransactionSearchParams;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.TransactionRepository;
import com.telerikacademy.virtualwallet.services.contracts.TransactionService;
import com.telerikacademy.virtualwallet.services.contracts.WalletService;
import com.telerikacademy.virtualwallet.validation.helper.ValidateTransaction;
import com.telerikacademy.virtualwallet.validation.helper.ValidateUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final WalletService walletService;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, WalletService walletService) {
        this.transactionRepository = transactionRepository;
        this.walletService = walletService;
    }

    @Override
    public Transaction getById(int id) {
        return transactionRepository.getById(id);
    }

    @Override
    public List<Transaction> getAll() {
        return transactionRepository.getAll();
    }

    public List<Transaction> getAllUserTransactions(String username) {
        return transactionRepository.getAllUserTransactions(username);
    }

    @Override
    public List<Transaction> filter(TransactionSearchParams searchParameters, User initiator) {
        ValidateUser.validateIsAdminOrCustomer(initiator);

        return transactionRepository.filter(searchParameters);
    }

    @Override
    public void create(Transaction transaction, User initiator) {
        ValidateUser.validateIsAdminOrCustomer(initiator);
        ValidateUser.validateIsUnblocked(initiator);
        ValidateTransaction.validateWalletBalance(transaction);

        boolean duplicateExists = true;

        try {
            transactionRepository.getById(transaction.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Transaction", "id", String.valueOf(transaction.getId()));
        }

        transactionRepository.create(transaction);
        walletService.withdraw(transaction.getSender(), transaction.getAmount());
        walletService.deposit(transaction.getReceiver(), transaction.getAmount());
    }

}
