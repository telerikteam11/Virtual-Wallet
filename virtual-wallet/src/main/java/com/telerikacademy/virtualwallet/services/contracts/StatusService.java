package com.telerikacademy.virtualwallet.services.contracts;

import com.telerikacademy.virtualwallet.models.entity.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(int id);

}


