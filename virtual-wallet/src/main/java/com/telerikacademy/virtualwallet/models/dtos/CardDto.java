package com.telerikacademy.virtualwallet.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class CardDto {

    private static final int CARD_NUMBER_MIN_LENGTH = 2;
    private static final int CARD_NUMBER_MAX_LENGTH = 16;
    private static final String CARD_NUMBER_BLANK_ERROR_MESSAGE = "Card number can't be empty!";
    private static final String CARD_NUMBER_LENGTH_ERROR_MESSAGE = "Card number must be 16 digits!";
    private static final String CARD_NUMBER_PATTERN_ERROR_MESSAGE = "Card number must contain only digits!";

    private static final int CARD_HOLDER_MIN_LENGTH = 2;
    private static final int CARD_HOLDER_MAX_LENGTH = 30;
    private static final String CARD_HOLDER_EMPTY_ERROR_MESSAGE = "Holder name can't be empty!";
    private static final String CARD_HOLDER_LENGTH_ERROR_MESSAGE = "Card holder must be between 2 and 30 digits!";

    private static final int SECURITY_NUMBER_MIN_LENGTH = 3;
    private static final int SECURITY_NUMBER_MAX_LENGTH = 3;
    private static final String SECURITY_NUMBER_EMPTY_ERROR_MESSAGE = "Security number can`t be null!";
    private static final String SECURITY_NUMBER_LENGTH_ERROR_MESSAGE = "Security number must be 3 digits!";
    private static final String SECURITY_NUMBER_PATTERN_ERROR_MESSAGE = "Security number must contains only digits!";

    private static final String EXPIRATION_DATE_EMPTY_ERROR_MESSAGE = "Expiration date can`t be null!";

    @NotBlank(message = CARD_NUMBER_BLANK_ERROR_MESSAGE)
    @Size(min = CARD_NUMBER_MIN_LENGTH, max = CARD_NUMBER_MAX_LENGTH, message = CARD_NUMBER_LENGTH_ERROR_MESSAGE)
    @Pattern(regexp = "\\d{16}", message = CARD_NUMBER_PATTERN_ERROR_MESSAGE)
    private String cardNumber;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate expirationDate;

    @NotBlank(message = CARD_HOLDER_EMPTY_ERROR_MESSAGE)
    @Size(min = CARD_HOLDER_MIN_LENGTH, max = CARD_HOLDER_MAX_LENGTH, message = CARD_HOLDER_LENGTH_ERROR_MESSAGE)
    private String cardHolder;

    @NotBlank(message = SECURITY_NUMBER_EMPTY_ERROR_MESSAGE)
    @Size(min = SECURITY_NUMBER_MIN_LENGTH, max = SECURITY_NUMBER_MAX_LENGTH, message = SECURITY_NUMBER_LENGTH_ERROR_MESSAGE)
    @Pattern(regexp = "\\d{3}", message = SECURITY_NUMBER_PATTERN_ERROR_MESSAGE)
    private String checkNumber;

    @Positive (message = "Id should be positive.")
    private int userId;


    public CardDto() {
    }

    public CardDto(String cardNumber, LocalDate expirationDate, String cardHolder, String checkNumber,int userId) {
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cardHolder = cardHolder;
        this.checkNumber = checkNumber;
        this.userId = userId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
