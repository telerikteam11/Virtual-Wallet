package com.telerikacademy.virtualwallet.models.entity;

import java.time.LocalDate;

public class TransactionSearchParams {

private LocalDate fromDate;
private LocalDate toDate;
private String sender;
private String receiver;
private String orderBy;

    public TransactionSearchParams() {
    }

    public TransactionSearchParams(LocalDate fromDate, LocalDate toDate, String sender, String receiver, String orderBy) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.sender = sender;
        this.receiver = receiver;
        this.orderBy = orderBy;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
