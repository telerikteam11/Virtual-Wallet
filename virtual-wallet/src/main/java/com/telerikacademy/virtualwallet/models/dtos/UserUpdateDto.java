package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.*;

public class UserUpdateDto {

    private static final int PASSWORD_MIN_LENGTH = 8;
    private static final String PASSWORD_SIZE_ERROR_MESSAGE = "Password should be at least 8 symbols";
    private static final String PASSWORD_PATTERN = "(^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$)";
    private static final String PASSWORD_PATTERN_ERROR_MESSAGE =
            "Password should contain capital letter, digit and special symbol (+, -, *, &, ^, …)";

    private static final String PHONE_NUMBER_PATTERN = "(^0[0-9]{9}$)";
    private static final String PHONE_NUMBER_EMPTY_ERROR_MESSAGE = "Phone number can`t be null!";
    private static final String PHONE_NUMBER_PATTERN_ERROR_MESSAGE = "Phone should be 10 symbols, starting with 0.";

    private String username;

    @Size(min = PASSWORD_MIN_LENGTH, message = PASSWORD_SIZE_ERROR_MESSAGE)
    @Pattern(regexp = PASSWORD_PATTERN, message = PASSWORD_PATTERN_ERROR_MESSAGE)
    @NotEmpty(message = "Password cannot be empty!")
    private String password;

    @Email(message = "Please enter a valid email!")
    private String email;

    @NotBlank(message = PHONE_NUMBER_EMPTY_ERROR_MESSAGE)
    @Pattern(regexp = PHONE_NUMBER_PATTERN, message = PHONE_NUMBER_PATTERN_ERROR_MESSAGE)
    @Size(min = 10, max = 10, message = "Phone number must be 10 digits!")
    private String phoneNumber;

    private int roleId;

    private int statusId;

    private String photo;

    public UserUpdateDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
