package com.telerikacademy.virtualwallet.models.mappers;

import com.telerikacademy.virtualwallet.models.dtos.TransferDto;
import com.telerikacademy.virtualwallet.models.entity.Transfer;
import com.telerikacademy.virtualwallet.repositories.contracts.CardRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.TransferRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class TransferMapper {

    private final CardRepository cardRepository;
    private final WalletRepository walletRepository;

    @Autowired
    public TransferMapper(CardRepository cardRepository, WalletRepository walletRepository) {
        this.cardRepository = cardRepository;
        this.walletRepository = walletRepository;
    }

    public Transfer fromDto(TransferDto dto) {
        Transfer transfer = new Transfer();
        transfer.setAmount(dto.getAmount());
        transfer.setCard(cardRepository.getById(dto.getCardId()));
        transfer.setWallet(walletRepository.getById(dto.getWalletId()));
        transfer.setDate(LocalDate.now());

        return transfer;
    }

    public TransferDto toDto(Transfer transfer) {
        TransferDto dto = new TransferDto();
        dto.setAmount(transfer.getAmount());
        dto.setCardId(transfer.getCard().getId());
        dto.setWalletId(transfer.getWallet().getId());

        return dto;
    }
}
