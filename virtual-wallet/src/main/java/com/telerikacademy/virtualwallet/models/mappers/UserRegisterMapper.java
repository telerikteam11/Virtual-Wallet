package com.telerikacademy.virtualwallet.models.mappers;

import com.telerikacademy.virtualwallet.models.dtos.RegisterDto;
import com.telerikacademy.virtualwallet.models.entity.Role;
import com.telerikacademy.virtualwallet.models.entity.Status;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.RoleRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.StatusRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterMapper {

    private static final String DEFAULT_USER_PHOTO = "profile-0-default.png";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final StatusRepository statusRepository;

    @Autowired
    public UserRegisterMapper(UserRepository userRepository, RoleRepository roleRepository, StatusRepository statusRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.statusRepository = statusRepository;
    }

    public User fromDto(RegisterDto userDto) {
        User user = new User();
        Role role = roleRepository.getById(1);
        Status status = statusRepository.getById(1);

        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setRole(role);
        user.setStatus(status);
        user.setPhoto(DEFAULT_USER_PHOTO);

        return user;
    }

    public User fromDto(RegisterDto userDto, int id) {

        User user = userRepository.getById(id);
        Role role = roleRepository.getById(1);
        Status status = statusRepository.getById(1);

        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setRole(role);
        user.setStatus(status);
        user.setPhoto(DEFAULT_USER_PHOTO);

        return user;
    }

    public RegisterDto toMvcDto(User user) {
        RegisterDto updateDto = new RegisterDto();

        updateDto.setUsername(user.getUsername());
        updateDto.setEmail(user.getEmail());
        updateDto.setPassword(user.getPassword());
        updateDto.setPasswordConfirm(user.getPassword());
        updateDto.setPhoneNumber((user.getPhoneNumber()));

        return updateDto;
    }

}
