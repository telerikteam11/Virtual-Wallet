package com.telerikacademy.virtualwallet.models.mappers;

import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.dtos.UserDto;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UserMapper {

    private final UserRepository userRepository;

    @Autowired
    public UserMapper(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User fromDto(UserDto userDto){
        User user = new User();
        dtoToObject(userDto, user);

        return user;
    }

    public User fromDto(UserDto userDto, int id){
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);

        return user;
    }

    public UserDto objectToDto(User user) {
        UserDto dto = new UserDto();
        dto.setEmail(user.getEmail());
        dto.setUsername(user.getUsername());
        dto.setPassword(user.getPassword());
        dto.setPhoneNumber(user.getPhoneNumber());
        return dto;
    }

    private void dtoToObject(UserDto userDto, User user){
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
    }

}
