package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.*;

public class RegisterDto {

    private static final int USERNAME_MIN_LENGTH = 2;
    private static final int USERNAME_MAX_LENGTH = 20;
    private static final String USERNAME_BLANK_ERROR_MESSAGE = "Username can't be empty";
    private static final String USERNAME_LENGTH_ERROR_MESSAGE = "Name should be between 2 and 20 symbols";

    private static final int PASSWORD_MIN_LENGTH = 8;
    private static final String PASSWORD_SIZE_ERROR_MESSAGE = "Password should be at least 8 symbols";
    private static final String PASSWORD_PATTERN = "(^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$)";
    private static final String PASSWORD_PATTERN_ERROR_MESSAGE =
            "Password should contain capital letter, digit and special symbol (+, -, *, &, ^, …)";

    private static final String PHONE_NUMBER_PATTERN = "(^0[0-9]{9}$)";
    private static final String PHONE_NUMBER_EMPTY_ERROR_MESSAGE = "Phone number can`t be null!";
    private static final String PHONE_NUMBER_PATTERN_ERROR_MESSAGE = "Phone should be 10 symbols, starting with 0.";

    @NotBlank(message = USERNAME_BLANK_ERROR_MESSAGE)
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = USERNAME_LENGTH_ERROR_MESSAGE)
    private String username;

    @Size(min = PASSWORD_MIN_LENGTH, message = PASSWORD_SIZE_ERROR_MESSAGE)
    @Pattern(regexp = PASSWORD_PATTERN, message = PASSWORD_PATTERN_ERROR_MESSAGE)
    @NotEmpty(message = "Password cannot be empty!")
    private String password;

    @NotEmpty(message = "Password confirmation cannot be empty!")
    private String passwordConfirm;

    @Email(message = "Please enter a valid email!")
    private String email;

    @NotBlank(message = PHONE_NUMBER_EMPTY_ERROR_MESSAGE)
    @Pattern(regexp = PHONE_NUMBER_PATTERN, message = PHONE_NUMBER_PATTERN_ERROR_MESSAGE)
    @Size(min = 10, max = 10, message = "Phone number must be 10 digits!")
    private String phoneNumber;

    public RegisterDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
