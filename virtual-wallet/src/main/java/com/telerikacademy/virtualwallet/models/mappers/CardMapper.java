package com.telerikacademy.virtualwallet.models.mappers;

import com.telerikacademy.virtualwallet.models.dtos.CardDto;
import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.CardRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardMapper {

    private final CardRepository cardRepository;
    private final UserRepository userRepository;

    @Autowired
    public CardMapper(CardRepository cardRepository, UserRepository userRepository) {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
    }

    public Card fromDto(CardDto cardDto) {
        Card card = new Card();
        dtoToObject(cardDto, card);

        return card;
    }

    public Card fromDto(CardDto cardDto, int id) {
        Card card = cardRepository.getById(id);
        dtoToObject(cardDto, card);

        return card;
    }

    private void dtoToObject(CardDto cardDto, Card card) {
        User user = userRepository.getById(cardDto.getUserId());
        card.setCardNumber(cardDto.getCardNumber());
        card.setCheckNumber(cardDto.getCheckNumber());
        card.setExpirationDate(cardDto.getExpirationDate());
        card.setCardHolder(cardDto.getCardHolder());
        card.setUser(user);

    }


    public CardDto toDto(Card card) {
        CardDto dto = new CardDto();
        dto.setCardHolder(card.getCardHolder());
        dto.setCardNumber(card.getCardNumber());
        dto.setExpirationDate(card.getExpirationDate());
        dto.setCheckNumber(card.getCheckNumber());
        dto.setUserId(card.getUser().getId());

        return dto;
    }

}
