package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.Positive;

public class WalletDto {

    @Positive
    private int userId;

    private double balance;

    public WalletDto() {
    }

    public WalletDto(int userId, double balance) {
        this.userId = userId;
        this.balance = balance;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
