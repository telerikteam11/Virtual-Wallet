package com.telerikacademy.virtualwallet.models.mappers;

import com.telerikacademy.virtualwallet.models.dtos.WalletDto;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.models.entity.Wallet;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WalletMapper {

    private final WalletRepository walletRepository;
    private final UserRepository userRepository;

    @Autowired
    public WalletMapper(WalletRepository walletRepository, UserRepository userRepository) {
        this.walletRepository = walletRepository;
        this.userRepository = userRepository;
    }

    public Wallet fromDto(WalletDto walletDto) {
        Wallet wallet = new Wallet();
        dtoToObject(walletDto, wallet);

        return wallet;
    }

    public Wallet fromDto(WalletDto walletDto, int id) {
        Wallet wallet = walletRepository.getById(id);
        dtoToObject(walletDto, wallet);

        return wallet;
    }

    private void dtoToObject(WalletDto walletDto, Wallet wallet) {
        User user = userRepository.getById(walletDto.getUserId());
        wallet.setUser(user);
        wallet.setBalance(walletDto.getBalance());
    }

    public WalletDto toDto(Wallet wallet) {
        WalletDto dto = new WalletDto();
        dto.setUserId(wallet.getUser().getId());
        dto.setBalance(wallet.getBalance());

        return dto;
    }
}
