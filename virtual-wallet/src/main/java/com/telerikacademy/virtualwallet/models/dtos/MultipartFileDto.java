package com.telerikacademy.virtualwallet.models.dtos;

import org.springframework.web.multipart.MultipartFile;

public class MultipartFileDto {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
