package com.telerikacademy.virtualwallet.models.mappers;

import com.telerikacademy.virtualwallet.models.dtos.UserUpdateDto;
import com.telerikacademy.virtualwallet.models.entity.Role;
import com.telerikacademy.virtualwallet.models.entity.Status;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.RoleRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.StatusRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateMvcMapper {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final StatusRepository statusRepository;

    @Autowired
    public UserUpdateMvcMapper(UserRepository userRepository, RoleRepository roleRepository, StatusRepository statusRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.statusRepository = statusRepository;
    }

    public User fromDto(UserUpdateDto userDto, int id) {

        User user = userRepository.getById(id);
        Role role = roleRepository.getById(user.getRole().getId());
        Status status = statusRepository.getById(user.getStatus().getId());

        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setRole(role);
        user.setStatus(status);
        user.setPhoto(userDto.getPhoto());

        return user;
    }

    public UserUpdateDto toMvcDto(User user) {
        UserUpdateDto updateDto = new UserUpdateDto();

        updateDto.setEmail(user.getEmail());
        updateDto.setPassword(user.getPassword());
        updateDto.setPhoneNumber((user.getPhoneNumber()));
        updateDto.setUsername(user.getUsername());
        updateDto.setRoleId(user.getRole().getId());
        updateDto.setStatusId(user.getStatus().getId());
        updateDto.setPhoto(user.getPhoto());

        return updateDto;
    }
}
