package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.Positive;

public class TransactionDto {

    private static final String SENDER_POSITIVE_ERROR_MESSAGE = "Sender Id must be positive number!";

    private static final String RECEIVER_POSITIVE_ERROR_MESSAGE = "Receiver Id must be positive number!";

    private static final String AMOUNT_POSITIVE_ERROR_MESSAGE = "Amount must be positive number!";

    @Positive(message = SENDER_POSITIVE_ERROR_MESSAGE)
    private int senderId;

    @Positive(message = RECEIVER_POSITIVE_ERROR_MESSAGE)
    private int receiverId;

    @Positive(message = AMOUNT_POSITIVE_ERROR_MESSAGE)
    private double amount;


    public TransactionDto() {
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }
}