package com.telerikacademy.virtualwallet.models.mappers;

import com.telerikacademy.virtualwallet.models.dtos.TransactionDto;
import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.Wallet;
import com.telerikacademy.virtualwallet.repositories.contracts.TransactionRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class TransactionMapper {

    private final WalletRepository walletRepository;

    @Autowired
    public TransactionMapper(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    public Transaction fromDto(TransactionDto transactionDto) {
        Transaction transaction = new Transaction();
        dtoToObject(transactionDto, transaction);
        return transaction;
    }

    public Transaction fromDto(TransactionDto transactionDto, Wallet sender) {
        Transaction transaction = new Transaction();
        dtoToObject(transactionDto, transaction);
        transaction.setSender(sender);
        return transaction;
    }

    private void dtoToObject(TransactionDto transactionDto, Transaction transaction) {
        Wallet sender = walletRepository.getById(transactionDto.getSenderId());
        Wallet receiver = walletRepository.getById(transactionDto.getReceiverId());

        transaction.setDate(LocalDate.now());
        transaction.setSender(sender);
        transaction.setReceiver(receiver);
        transaction.setAmount(transactionDto.getAmount());
    }

}
