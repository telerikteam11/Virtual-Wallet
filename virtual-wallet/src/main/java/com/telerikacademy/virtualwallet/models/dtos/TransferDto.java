package com.telerikacademy.virtualwallet.models.dtos;

import javax.validation.constraints.Positive;

public class TransferDto {

    @Positive(message = "Card ID should be positive!")
    private int cardId;

    @Positive(message = "Wallet ID should be positive!")
    private int walletId;

    @Positive(message = "Amount should be positive!")
    private double amount;

    public TransferDto() {
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}

