package com.telerikacademy.virtualwallet.repositories.contracts;

import com.telerikacademy.virtualwallet.models.entity.Card;

import java.util.List;

public interface CardRepository extends BaseModifyRepository<Card> {

//    void create(Card card);
//
//    void update(Card card);
//
//    void delete(int id);
//
//    List<Card> getAll();
//
//    Card getById(int id);
//
//    Card getByCardNumber(String cardNumber);
//
//    Card getByHolderName(String holderName);
//
    List<Card> getAllUserCards(String username);
}
