package com.telerikacademy.virtualwallet.repositories;


import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.repositories.contracts.TransactionRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.TransferRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.Transfer;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TransferRepositoryImpl extends BaseModifyRepositoryImpl <Transfer> implements TransferRepository {

    @Autowired
    public TransferRepositoryImpl(SessionFactory sessionFactory) {
        super(Transfer.class, sessionFactory);
    }


    @Override
    public List<Transfer> getAllUserTransfers(String username) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Transfer where card.user.username = :username";

            Query<Transfer> query = session.createQuery(queryString, Transfer.class);
            query.setParameter("username", username);

            return query.list();
        }
    }

}

