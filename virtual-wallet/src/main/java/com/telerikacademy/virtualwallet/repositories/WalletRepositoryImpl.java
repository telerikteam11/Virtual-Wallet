package com.telerikacademy.virtualwallet.repositories;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.Wallet;
import com.telerikacademy.virtualwallet.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.WalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class WalletRepositoryImpl extends BaseModifyRepositoryImpl <Wallet> implements BaseModifyRepository<Wallet>, WalletRepository {

    @Autowired
    public WalletRepositoryImpl(SessionFactory sessionFactory) {
        super(Wallet.class, sessionFactory);
    }


    @Override
    public List<Wallet> getAllUserWallets(String username) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Wallet where user.username = :username";

            Query<Wallet> query = session.createQuery(queryString, Wallet.class);
            query.setParameter("username", username);

            return query.list();
        }
    }

}
