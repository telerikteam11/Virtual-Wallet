package com.telerikacademy.virtualwallet.repositories;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {


    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }


    @Override
    public List<User> search(Optional<String> keyword) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where ((:keyword = '%%') OR " +
                    "(username like :keyword) OR " +
                    "(email like :keyword) OR " +
                    "(phoneNumber like :keyword))", User.class);
            query.setParameter("keyword", "%" + keyword.orElse("") + "%");

            return query.list();
        }
    }

    @Override
    public User findByVerificationCode(String code) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where verificationCode = :code", User.class);
            query.setParameter("code", code);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "code", code);
            }

            return users.get(0);
        }
    }

}
