package com.telerikacademy.virtualwallet.repositories.contracts;

import java.util.List;

public interface BaseGetRepository<T> {
    List<T> getAll();

    <V> T getByField(String fieldName, V fieldValue);

//    <V> List<T> getAllByField(String fieldName, V value);

    T getById(int id);
}
