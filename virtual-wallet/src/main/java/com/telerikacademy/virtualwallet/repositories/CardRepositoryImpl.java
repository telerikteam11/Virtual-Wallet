package com.telerikacademy.virtualwallet.repositories;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.virtualwallet.repositories.contracts.CardRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardRepositoryImpl extends BaseModifyRepositoryImpl <Card> implements CardRepository {

    @Autowired
    public CardRepositoryImpl(SessionFactory sessionFactory) {
        super(Card.class, sessionFactory);
    }

    @Override
    public List<Card> getAllUserCards(String username) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Card where user.username = :username";

            Query<Card> query = session.createQuery(queryString, Card.class);
            query.setParameter("username", username);

            return query.list();
        }
    }

}
