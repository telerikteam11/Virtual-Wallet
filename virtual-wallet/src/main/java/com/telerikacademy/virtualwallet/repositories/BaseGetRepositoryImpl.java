package com.telerikacademy.virtualwallet.repositories;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.repositories.contracts.BaseGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.lang.String.format;

@Repository
public abstract class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    public final Class<T> clazz;
    public final SessionFactory sessionFactory;

    @Autowired
    public BaseGetRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<T> query = session.createQuery("from " + clazz.getSimpleName(), clazz);

            return query.list();
        }
    }

//    @Override
//    public <V> List<T> getAllByField(String fieldName, V value) {
//        try (Session session = sessionFactory.openSession()) {
//            Query<T> query = session.createQuery(format("from %s where %s = :%s",
//                    clazz.getSimpleName(), fieldName, fieldName), clazz);
//            query.setParameter(fieldName, value);
//            var result = query.list();
//            if (result.size() == 0) {
//                throw new EntityNotFoundException(clazz.getSimpleName(), fieldName, value.toString());
//            }
//            return result;
//        }
//    }

    @Override
    public <V> T getByField(String fieldName, V fieldValue) {
        try (Session session = sessionFactory.openSession()) {
            Query<T> query = session.createQuery(String.format("from %s where %s = '%s'", clazz.getSimpleName(),
                    fieldName, fieldValue), clazz);

            List<T> result = query.list();

            //ToDo use validationHelper
            if (result.size() == 0) {
                throw new EntityNotFoundException(clazz.getSimpleName(), fieldName, fieldValue.toString());
            }

            return result.get(0);
        }
    }

    @Override
    public T getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            T object = session.get(clazz, id);
            //ToDo -> Use validationHelper
            if (object == null) {
                throw new EntityNotFoundException(clazz.getSimpleName(), id);
            }

            return object;
        }
    }

}