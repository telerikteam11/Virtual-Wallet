package com.telerikacademy.virtualwallet.repositories.contracts;

import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.Wallet;

import java.util.List;

public interface WalletRepository extends BaseModifyRepository<Wallet> {

//    void create(Wallet wallet);
//
//    void update(Wallet wallet);
//
//    void delete(int id);
//
//    List<Wallet> getAll();
//
//    Wallet getById(int id);

//    Wallet getByUserId(int userId);
//
    List<Wallet> getAllUserWallets(String username);


}
