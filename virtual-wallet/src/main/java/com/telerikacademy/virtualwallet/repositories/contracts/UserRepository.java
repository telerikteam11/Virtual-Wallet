package com.telerikacademy.virtualwallet.repositories.contracts;

import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseModifyRepository<User>{

//    void create(User user);
//
//    void update(User user);
//
//    void delete(int id);
//
//    List<User> getAll();
//
//    User getById(int id);
//
//    User getByUsername(String username);
//
//    User getByEmail(String email);
//
//    User getByPhoneNumber(String phoneNumber);

    List<User> search(Optional<String> keyword);

    User findByVerificationCode(String code);

}
