package com.telerikacademy.virtualwallet.repositories.contracts;

import com.telerikacademy.virtualwallet.models.entity.Status;

public interface StatusRepository extends BaseGetRepository<Status>{
}
