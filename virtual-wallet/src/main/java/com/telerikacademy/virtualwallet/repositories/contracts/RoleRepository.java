package com.telerikacademy.virtualwallet.repositories.contracts;

import com.telerikacademy.virtualwallet.models.entity.Role;
import com.telerikacademy.virtualwallet.models.entity.Status;

import java.util.List;

public interface RoleRepository extends BaseGetRepository<Role> {

}
