package com.telerikacademy.virtualwallet.repositories;

import com.telerikacademy.virtualwallet.models.entity.Status;
import com.telerikacademy.virtualwallet.repositories.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StatusRepositoryImpl extends BaseGetRepositoryImpl<Status> implements StatusRepository {

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(Status.class, sessionFactory);
    }

}
