package com.telerikacademy.virtualwallet.repositories;

import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.TransactionSearchParams;
import com.telerikacademy.virtualwallet.repositories.contracts.TransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class TransactionRepositoryImpl extends BaseModifyRepositoryImpl <Transaction> implements TransactionRepository {

    @Autowired
    public TransactionRepositoryImpl(SessionFactory sessionFactory) {
        super(Transaction.class, sessionFactory);
    }

    @Override
    public List<Transaction> getAllUserTransactions(String username) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Transaction where sender.user.username = :username or receiver.user.username = :username";

            Query<Transaction> query = session.createQuery(queryString, Transaction.class);
            query.setParameter("username", username);

            return query.list();
        }
    }


    @Override
    public List<Transaction> filter(TransactionSearchParams searchParameters) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryStr = new StringBuilder();
            queryStr.append("from Transaction where ");

            LocalDate fromDate = searchParameters.getFromDate();
            if (fromDate == null)
                fromDate = LocalDate.EPOCH;

            LocalDate toDate = searchParameters.getToDate();
            if (toDate == null)
                toDate = LocalDate.now();

            queryStr.append(" (date between :fromDate and :toDate)");

            if (searchParameters.getSender() != null && !searchParameters.getSender().isBlank()) {
                queryStr.append(" and lcase(sender.user.username) like concat('%',:sender,'%') ");
            }
            if (searchParameters.getReceiver() != null && !searchParameters.getReceiver().isBlank()) {
                queryStr.append(" and lcase(receiver.user.username) like concat('%',:receiver,'%') ");
            }
            if (searchParameters.getOrderBy() != null
                    && !searchParameters.getOrderBy().isBlank()
                    && !searchParameters.getOrderBy().equals("-1")) {
                switch (searchParameters.getOrderBy()) {
                    case "amount":
                        queryStr.append(" ORDER BY amount ASC");
                        break;
                    case "amount_desc":
                        queryStr.append(" ORDER BY amount DESC");
                        break;
                    case "date":
                        queryStr.append(" ORDER BY date ASC");
                        break;
                    case "date_desc":
                        queryStr.append(" ORDER BY date DESC");
                        break;
                }
            }

            Query<Transaction> query = session.createQuery(queryStr.toString(), Transaction.class);

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);

            if (searchParameters.getSender() != null && !searchParameters.getSender().isBlank()) {
                query.setParameter("sender", searchParameters.getSender().toLowerCase());
            }

            if (searchParameters.getReceiver() != null && !searchParameters.getReceiver().isBlank()) {
                query.setParameter("receiver", searchParameters.getReceiver().toLowerCase());
            }

            return query.list();
        }
    }

}
