package com.telerikacademy.virtualwallet.repositories.contracts;

import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.Transfer;

import java.util.List;

public interface TransferRepository extends BaseModifyRepository<Transfer> {

//    void create(Transfer transfer);
//
//    List<Transfer> getAll();
//
//    Transfer getById(int id);
//
    List<Transfer> getAllUserTransfers(String username);


}
