package com.telerikacademy.virtualwallet.repositories.contracts;

import com.telerikacademy.virtualwallet.models.entity.Card;
import com.telerikacademy.virtualwallet.models.entity.Transaction;
import com.telerikacademy.virtualwallet.models.entity.TransactionSearchParams;
import com.telerikacademy.virtualwallet.models.entity.User;

import java.util.List;

public interface TransactionRepository extends BaseModifyRepository<Transaction>{
//    Transaction getById(int id);
//
//    List<Transaction> getAll();
//
//    void create(Transaction transaction);
//
    List<Transaction> getAllUserTransactions(String username);

    List<Transaction> filter(TransactionSearchParams searchParameters);
}
