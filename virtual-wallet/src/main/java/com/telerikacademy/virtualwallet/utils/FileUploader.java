package com.telerikacademy.virtualwallet.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileUploader {
    void uploadFile(MultipartFile file) throws IOException;

    String getFile(String path) throws IOException;
}
