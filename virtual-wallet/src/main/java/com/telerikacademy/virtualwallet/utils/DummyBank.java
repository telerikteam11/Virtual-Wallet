package com.telerikacademy.virtualwallet.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DummyBank {

    public static int dummyTransfer(String username, String cardNumber, String cardCheck, String cardExpDate, String amount) throws IOException {
        URL url = new URL("http://localhost:8080/api/bankdummy");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Authorization", username);
        con.setDoOutput(true);
        String jsonInputString = String.format
                ("{\"cardNumber\": \"%s\"," +
                        " \"cardCheck\": \"%s\"," +
                        " \"expirationDate\": \"%s\"," +
                        " \"amount\": %s}",cardNumber,cardCheck,cardExpDate,amount);

        try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        return con.getResponseCode();
    }
}
