package com.telerikacademy.virtualwallet.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

@Component
public class FileUploaderImpl implements FileUploader {

    @Override
    public void uploadFile(MultipartFile file) throws IOException {
        file.transferTo(new File(System.getProperty("user.dir") +
                "\\virtual-wallet\\src\\main\\resources\\static\\assets\\img\\illustrations\\profiles\\" + file.getOriginalFilename()));
    }

    @Override
    public String getFile(String path) throws IOException {
        File file = new File(System.getProperty("user.dir") +
                "\\virtual-wallet\\src\\main\\resources\\static\\assets\\img\\illustrations\\profiles\\" + path);
        Scanner sc = new Scanner(file);
        StringBuilder str = new StringBuilder();

        while (sc.hasNextLine()) {
            str.append(sc.nextLine()).append("\n");
        }

        return str.toString();
    }
}
