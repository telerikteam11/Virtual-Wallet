package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.InsufficientFundsException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.entity.*;
import com.telerikacademy.virtualwallet.repositories.contracts.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.virtualwallet.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceImplTests {

    @Mock
    TransactionRepository mockTransactionRepository;

    @Mock
    WalletServiceImpl mockWalletService;

    @InjectMocks
    TransactionServiceImpl mockTransactionService;


    @Test
    public void getById_Should_CallRepository() {
        //Arrange
        Transaction mockTransaction = createMockTransaction();
        Mockito.when(mockTransactionRepository.getById(1))
                .thenReturn(mockTransaction);

        // Act
        mockTransactionService.getById(1);

        // Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .getById(mockTransaction.getId());
    }

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockTransactionRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        mockTransactionService.getAll();

        //Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getAllUserTransactions_should_callRepository() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockTransactionRepository.getAllUserTransactions(mockUser.getUsername()))
                .thenReturn(new ArrayList<>());

        //Act
        mockTransactionService.getAllUserTransactions(mockUser.getUsername());

        //Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .getAllUserTransactions(mockUser.getUsername());
    }

    @Test
    void filter_should_Throw_when_UserIsNotAuthenticated() {
        // Arrange
        User user = createMockUser();
        user.setRole(new Role(3, "Fake"));
        TransactionSearchParams mockTransactionSearchParams = new TransactionSearchParams();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockTransactionService.filter(mockTransactionSearchParams, user));
    }

    @Test
    void filter_should_callRepository() {
        // Arrange
        TransactionSearchParams mockTransactionSearchParams = new TransactionSearchParams();
        Mockito.when(mockTransactionRepository.filter(mockTransactionSearchParams))
                .thenReturn(new ArrayList<>());

        // Act
        mockTransactionService.filter(mockTransactionSearchParams, createMockUser());
        // Assert

        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .filter(mockTransactionSearchParams);
    }

    @Test
    void create_should_Throw_when_UserIsNotAuthenticated() {
        // Arrange
        User user = createMockUser();
        user.setRole(new Role(3, "Fake"));
        Transaction mockTransaction = createMockTransaction();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockTransactionService.create(mockTransaction, user));
    }

    @Test
    void create_should_Throw_when_UserIsBlocked() {
        // Arrange
        User user = createMockUser();
        Status mockStatus = createBlockStatus();
        user.setStatus(mockStatus);
        Transaction mockTransaction = createMockTransaction();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockTransactionService.create(mockTransaction, user));
    }

    @Test
    void create_should_Throw_when_UserDoesNotHaveEnoughBalance() {
        // Arrange
        User user = createMockUser();
        Transaction mockTransaction = createMockTransaction();
        Wallet mockWallet = createMockWallet();

        mockWallet.setBalance(20);
        mockTransaction.setSender(mockWallet);
        mockTransaction.setAmount(200.2);

        //Act, Assert
        Assertions.assertThrows(InsufficientFundsException.class,
                () -> mockTransactionService.create(mockTransaction, user));
    }

    @Test
    void create_should_Throw_when_DuplicateExists() {
        // Arrange
        User user = createMockUser();
        Transaction mockTransaction = createMockTransaction();

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockTransactionService.create(mockTransaction, user));
    }

    @Test
    public void create_should_callRepository() {
        //Arrange
        Transaction mockTransaction = createMockTransaction();
        Wallet mockSender = createMockWallet();
        Wallet mockReceiver = createMockWallet();
        mockTransaction.setSender(mockSender);
        mockTransaction.setReceiver(mockReceiver);
        User mockUser = createMockUser();

        Mockito.when(mockTransactionRepository.getById(mockTransaction.getId()))
                .thenThrow(new EntityNotFoundException("Transaction", mockTransaction.getId()));

        //Act
        mockTransactionService.create(mockTransaction, mockUser);
        mockWalletService.withdraw(mockSender, 10);
        mockWalletService.deposit(mockReceiver, 10);

        //Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .create(mockTransaction);
    }

}
