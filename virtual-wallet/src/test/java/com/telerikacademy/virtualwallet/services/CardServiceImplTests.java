package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.entity.*;
import com.telerikacademy.virtualwallet.repositories.contracts.CardRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.virtualwallet.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CardServiceImplTests {

    @Mock
    CardRepository mockCardRepository;

    @InjectMocks
    CardServiceImpl mockCardService;

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockCardRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        mockCardService.getAll();

        //Assert
        Mockito.verify(mockCardRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange
        User user = createMockUser();
        Card card = createMockCard();
        Mockito.when(mockCardRepository.getById(1))
                .thenReturn(card);

        // Act
        mockCardService.getById(1, user);

        // Assert
        Mockito.verify(mockCardRepository, Mockito.times(1))
                .getById(card.getId());
    }

    @Test
    public void getAllUserCards_Should_CallRepository() {
        //Arrange
        User user = createMockUser();
        Mockito.when(mockCardRepository.getAllUserCards(user.getUsername()))
                .thenReturn(new ArrayList<>());

        //Act
        mockCardService.getAllUserCards(user.getUsername());

        //Assert
        Mockito.verify(mockCardRepository, Mockito.times(1))
                .getAllUserCards(user.getUsername());
    }

    @Test
    public void createCard_should_Throw_UserIsNotAuthenticated() {
        // Arrange
        User mockUser = createMockUser();
        mockUser.setRole(new Role(3, "Fake"));
        Card mockCard = createMockCard();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockCardService.create(mockCard, mockUser));
    }

    @Test
    void create_should_Throw_when_DuplicateExists() {
        // Arrange
        User mockUser = createMockUser();
        Card mockCard = createMockCard();

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockCardService.create(mockCard, mockUser));
    }

    @Test
    public void create_should_callRepository() {
        //Arrange
        User mockUser = createMockUser();
        Card mockCard = createMockCard();

        Mockito.when(mockCardRepository.getById(mockCard.getId()))
                .thenThrow(new EntityNotFoundException("Card", mockCard.getId()));

        //Act
        mockCardService.create(mockCard, mockUser);


        //Assert
        Mockito.verify(mockCardRepository, Mockito.times(1))
                .create(mockCard);
    }

    @Test
    public void update_Should_ThrowException_When_NotExists() {
        //Arrange
        User mockUser = createMockUser();
        Card mockCard = createMockCard();

        Mockito.when(mockCardRepository.getByField(mockCard.getCardNumber(), mockCard.getCardNumber()))
                .thenThrow(new EntityNotFoundException("Card", mockCard.getId()));

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockCardService.update(mockCard, mockUser));
    }

    @Test
    public void duplicateCardNumber_should_throw_when_cardWithSameCardNumberExists() {

        // Arrange
        User user = createMockUser();
        Card mockCard = createMockCard();

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockCardService.create(mockCard, user));
    }

    @Test
    public void update_Should_UpdateCard_When_Valid() {
        //Arrange
        User mockUser = createMockUser();
        Card mockCard = createMockCard();
        //Act
        mockCardService.update(mockCard, mockUser);
        //Assert
        Mockito.verify(mockCardRepository, Mockito.times(1))
                .update(mockCard);
    }

    @Test
    void delete_should_throwException_when_UserIsNotAuthenticated() {
        // Arrange
        Card mockCard = createMockCard();
        User mockUser = createMockUser();
        mockUser.setRole(new Role(3, "Fake"));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockCardService.delete(mockCard.getId(), mockUser));
    }

    @Test
    void delete_should_callRepository_when_UserIsNotAuthenticated() {
        // Arrange
        Card mockCard = createMockCard();
        User mockUser = createMockUser();
        mockUser.setId(mockCard.getId());

        mockCardService.delete(mockCard.getId(), mockUser);

        // Act, Assert
        Mockito.verify(mockCardRepository, Mockito.times(1))
                .delete(mockCard.getId());
    }

}
