package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.models.entity.Status;
import com.telerikacademy.virtualwallet.repositories.contracts.StatusRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.virtualwallet.Helpers.createUnblockStatus;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {

    @Mock
    StatusRepository mockStatusRepository;

    @InjectMocks
    StatusServiceImpl mockStatusService;

    @Test
    public void getAll_Should_callRepository() {
        //Arrange
        Mockito.when(mockStatusRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        mockStatusService.getAll();

        //Assert
        Mockito.verify(mockStatusRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange
        Status status = createUnblockStatus();
        Mockito.when(mockStatusRepository.getById(Mockito.anyInt()))
                .thenReturn(status);

        //Act
        mockStatusService.getById(1);

        //Assert
        Mockito.verify(mockStatusRepository, Mockito.times(1))
                .getById(status.getId());
    }
}
