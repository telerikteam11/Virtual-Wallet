package com.telerikacademy.virtualwallet.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class BankDummyServiceImplTests {

    @InjectMocks
    BankDummyServiceImpl service;

    @Test
    void depositMoney_should_returnFalse_when_numberIsOdd() {
        //Arrange, Act
        boolean result = service.depositMoney(LocalDate.now(), 1000);

        //Assert
        if (result) {
            Assertions.assertTrue(true);
        } else {
            Assertions.assertFalse(false);
        }
    }

}

