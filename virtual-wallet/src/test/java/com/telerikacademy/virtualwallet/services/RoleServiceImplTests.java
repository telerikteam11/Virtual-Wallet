package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.models.entity.Role;
import com.telerikacademy.virtualwallet.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.virtualwallet.Helpers.createCustomerRole;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RoleRepository mockRoleRepository;

    @InjectMocks
    RoleServiceImpl mockRoleService;

    @Test
    public void getAll_Should_callRepository() {
        //Arrange
        Mockito.when(mockRoleRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        mockRoleService.getAll();

        //Assert
        Mockito.verify(mockRoleRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange
        Role role = createCustomerRole();
        Mockito.when(mockRoleRepository.getById(Mockito.anyInt()))
                .thenReturn(role);

        //Act
        mockRoleService.getById(1);

        //Assert
        Mockito.verify(mockRoleRepository, Mockito.times(1))
                .getById(role.getId());
    }

}
