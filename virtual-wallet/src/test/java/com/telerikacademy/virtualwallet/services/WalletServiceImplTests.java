package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.entity.*;
import com.telerikacademy.virtualwallet.repositories.contracts.WalletRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.virtualwallet.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class WalletServiceImplTests {

    @Mock
    WalletRepository mockWalletRepository;

    @InjectMocks
    WalletServiceImpl mockWalletService;

    @Test
    public void getById_Should_CallRepository() {
        //Arrange
        Wallet mockWallet = createMockWallet();
        User mockUser = createMockUser();
        Mockito.when(mockWalletRepository.getById(mockWallet.getId()))
                .thenReturn(mockWallet);

        // Act
        mockWalletService.getById(1, mockUser);

        // Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .getById(mockWallet.getId());
    }

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockWalletRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        mockWalletService.getAll();

        //Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getByUserId_Should_CallRepository() {
        //Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = createMockWallet();
        Mockito.when(mockWalletRepository.getByField(Mockito.anyString(),Mockito.anyInt()))
                .thenReturn(mockWallet);

        // Act
        mockWalletService.getByUserId(mockUser.getId());

        // Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .getByField(Mockito.anyString(),Mockito.anyInt());
    }

    @Test
    public void getAllUserWallets_Should_CallRepository() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockWalletRepository.getAllUserWallets(mockUser.getUsername()))
                .thenReturn(new ArrayList<>());

        // Act
        mockWalletService.getAllUserWallets(mockUser.getUsername());

        // Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .getAllUserWallets(mockUser.getUsername());
    }

    @Test
    void create_should_Throw_when_UserIsNotAuthenticated() {
        // Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = createMockWallet();
        mockUser.setRole(new Role(3, "Fake"));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.create(mockWallet, mockUser));
    }

    @Test
    void create_should_Throw_when_DuplicateExists() {
        // Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = createMockWallet();

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockWalletService.create(mockWallet, mockUser));
    }

    @Test
    public void create_should_callRepository() {
        //Arrange
        Wallet mockWallet = createMockWallet();
        User mockUser = createMockUser();

        Mockito.when(mockWalletRepository.getById(mockWallet.getId()))
                .thenThrow(new EntityNotFoundException("Wallet", mockWallet.getId()));

        //Act
        mockWalletService.create(mockWallet, mockUser);

        //Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .create(mockWallet);
    }

    @Test
    public void update_Should_Withdraw_When_Valid() {
        //Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = createMockWallet();

        //Act
        mockWalletService.update(mockWallet, mockUser);

        //Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .update(mockWallet);
    }

    @Test
    public void update_Should_ThrowException_When_NotExists() {
        //Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = createMockWallet();

        Mockito.when(mockWalletRepository.getById(mockWallet.getId()))
                .thenThrow(new EntityNotFoundException("Wallet", mockWallet.getId()));

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockWalletService.update(mockWallet, mockUser));
    }

    @Test
    void delete_should_Throw_when_UserIsNotAuthenticated() {
        // Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = createMockWallet();
        mockUser.setRole(new Role(3, "Fake"));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.delete(1, mockUser));
    }

    @Test
    void delete_should_Throw_when_BalanceIsDifferentFromZero() {
        //Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = new Wallet(1, 1000);
        mockWallet.setUser(mockUser);

        Mockito.when(mockWalletRepository.getById(mockWallet.getId()))
                .thenReturn(mockWallet);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.delete(mockWallet.getId(), mockUser));
    }

    @Test
    void delete_should_delete() {
        //Arrange
        User mockUser = createMockUser();
        Wallet mockWallet = new Wallet(1, 0);
        mockWallet.setUser(mockUser);

        Mockito.when(mockWalletRepository.getById(mockWallet.getId()))
                .thenReturn(mockWallet);

        //Act
        mockWalletService.delete(mockWallet.getId(), mockUser);

        //Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .delete(mockWallet.getId());
    }
}
