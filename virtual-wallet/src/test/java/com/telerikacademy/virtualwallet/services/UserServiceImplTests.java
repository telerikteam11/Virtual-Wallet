package com.telerikacademy.virtualwallet.services;

import com.telerikacademy.virtualwallet.exceptions.DuplicateEntityException;
import com.telerikacademy.virtualwallet.exceptions.EntityNotFoundException;
import com.telerikacademy.virtualwallet.exceptions.UnauthorizedOperationException;
import com.telerikacademy.virtualwallet.models.entity.User;
import com.telerikacademy.virtualwallet.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.virtualwallet.Helpers.createMockAdmin;
import static com.telerikacademy.virtualwallet.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl mockUserService;

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        mockUserService.getAll(createMockAdmin());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void populate_should_calLRepository(){
        mockUserService.populate();
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getAll();
    }
    @Test
    void getByUsername_should_callRepository() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockUserRepository.getByField(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(mockUser);

        //Act
        mockUserService.getByUsername(mockUser.getUsername());

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getByField(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchExists() {
        //Arrange
        User mockUser = createMockAdmin();
        Mockito.when(mockUserRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        // Act
        User result = mockUserService.getById(1, mockUser);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getPhoneNumber(), result.getPhoneNumber()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole()),
                () -> Assertions.assertEquals(mockUser.getStatus(), result.getStatus()));
    }

    @Test
    void search_should_callRepository() {
        //Arrange
        Optional<String> mockKeyword = Optional.of("Mock");
        Mockito.when(mockUserRepository.search(mockKeyword))
                .thenReturn(new ArrayList<>());

        //Act
        mockUserService.search(mockKeyword);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .search(mockKeyword);
    }

    @Test
    public void create_Should_ThrowException_When_DuplicateExists() {
        //Arrange
        User mockUser = createMockUser();

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.create(mockUser));
    }

   /* @Test
    public void create_Should_CreateUser_When_Valid() {
        //Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(new EntityNotFoundException("User", mockUser.getId()));

        //Act, Assert
        mockUserService.create(mockUser);
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .create(mockUser);
    }*/

    @Test
    void update_should_throw_whenUserIsNotTheSame() {
        //Arrange
        User mockUser = createMockUser();
        User differentMockUser = createMockUser();
        differentMockUser.setUsername("Different");

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserService.update(mockUser, differentMockUser));
    }

    @Test
    void update_should_callRepository() {
        //Arrange
        User mockUser = createMockUser();
        //Act
        mockUserService.update(mockUser, mockUser);
        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void delete_should_callRepository() {
        //Arrange
        User mockAdmin = createMockAdmin();

        //Act
        mockUserService.delete(2, mockAdmin);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .delete(2);
    }

    @Test
    void updateStatus_Should_throw_when_userIsNotAdmin(){
        //Arrange
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        mockUser2.setId(2);
        mockUser2.setUsername("MockUser2");

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserService.updateStatus(mockUser, mockUser2));
    }

    @Test
    void updateStatus_should_callRepository(){
        //Arrange
        User mockAdmin = createMockAdmin();
        User mockUser = createMockUser();

        //Act
        mockUserService.updateStatus(mockUser, mockAdmin);

        //Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .update(mockUser);
    }

}
