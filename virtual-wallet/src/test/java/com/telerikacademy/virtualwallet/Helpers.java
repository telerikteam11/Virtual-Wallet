package com.telerikacademy.virtualwallet;

import com.telerikacademy.virtualwallet.models.entity.*;

import java.time.LocalDate;

public class Helpers {

    public static User createMockUser(Role role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setEmail("mock@user.com");
        mockUser.setPhoneNumber("0888888888");
        mockUser.setRole(role);
        mockUser.setStatus(createUnblockStatus());

        return mockUser;
    }

    public static Transaction createMockTransaction() {
        var mockTransaction = new Transaction();
        mockTransaction.setId(1);
        mockTransaction.setDate(LocalDate.now());
        mockTransaction.setSender(createMockWallet());
        mockTransaction.setReceiver(createMockWallet());
        mockTransaction.setAmount(10000.1);

        return mockTransaction;
    }

    public static Card createMockCard() {
        var mockCard = new Card();
        mockCard.setId(1);
        mockCard.setCardNumber("MockCardNumber");
        mockCard.setExpirationDate(LocalDate.MAX);
        mockCard.setCardHolder("MockCardHolder");
        mockCard.setCheckNumber("888");
        mockCard.setUser(createMockUser());

        return mockCard;
    }

    public static Wallet createMockWallet() {
        var mockWallet = new Wallet();
        mockWallet.setId(1);
        mockWallet.setUser(createMockUser());
        mockWallet.setBalance(10000.1);

        return mockWallet;
    }

    public static User createMockUser() {
        return createMockUser(createCustomerRole());
    }

    public static User createMockAdmin() {
        return createMockUser(createAdminRole());
    }

    public static Role createCustomerRole() {
        return new Role(1, "Customer");
    }

    public static Role createAdminRole() {
        return new Role(2, "Admin");
    }

    public static Status createUnblockStatus() {
        return new Status(1, "unblocked");
    }

    public static Status createBlockStatus() {
        return new Status(2, "blocked");
    }

}
